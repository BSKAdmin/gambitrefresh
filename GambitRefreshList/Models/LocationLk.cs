﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class LocationLk
    {
        public LocationLk()
        {
            Itassets = new HashSet<Itasset>();
        }

        public int Id { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Itasset> Itassets { get; set; }
    }
}
