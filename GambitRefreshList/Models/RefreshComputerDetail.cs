﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GambitRefreshList.Models
{
    public class RefreshComputerDetail : RefreshedComputer
    {

        public Itasset  Asset { get; set; }
       
        public RefreshComputerDetail(GambitContext context) 
        {
            this.Asset = context.Itassets.Find(this.AssetId);
        }

        public RefreshComputerDetail(GambitContext context, int id)
        {
            this.Asset = context.Itassets.Find(id);
        }

    }
}
