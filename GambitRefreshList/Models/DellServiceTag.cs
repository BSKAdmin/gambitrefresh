﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class DellServiceTag
    {
        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public string IpAddress { get; set; }
        public string OperatingSystem { get; set; }
        public string DeviceType { get; set; }
        public string Manufacturer { get; set; }
        public string Location { get; set; }
    }
}
