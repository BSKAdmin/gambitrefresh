﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class RefreshedComputer
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Size { get; set; }
        public string Model { get; set; }
        public string OperatingSystem { get; set; }
        public string ServiceTagNumber { get; set; }
        public DateTime? PurchasedOn { get; set; }
        public string DisplayName { get; set; }
        public string Branch { get; set; }
        public string Desciption { get; set; }
        public string SerialNumber { get; set; }
        public string Ds { get; set; }
        public int EmployeeId { get; set; }
        public int AssetId { get; set; }
    }
}
