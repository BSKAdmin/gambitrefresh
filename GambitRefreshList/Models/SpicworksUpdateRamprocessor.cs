﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class SpicworksUpdateRamprocessor
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string IpAddress { get; set; }
        public string LastLogin { get; set; }
        public string NumberOfProcessors { get; set; }
        public string ProcessorType { get; set; }
        public string RawProcessorType { get; set; }
        public string LastScanTime { get; set; }
        public string Memory { get; set; }
    }
}
