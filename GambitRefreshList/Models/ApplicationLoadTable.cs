﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class ApplicationLoadTable
    {
        public string DeviceName { get; set; }
        public string ApplicationName { get; set; }
        public string InstallationProductKey { get; set; }
    }
}
