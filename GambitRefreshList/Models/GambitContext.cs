﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class GambitContext : DbContext
    {
        public GambitContext()
        {
        }

        public GambitContext(DbContextOptions<GambitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ApplicationLoadTable> ApplicationLoadTables { get; set; }
        public virtual DbSet<AssetReplacementLookup> AssetReplacementLookups { get; set; }
        public virtual DbSet<AssignedAsset> AssignedAssets { get; set; }
        public virtual DbSet<AssignedSoftware> AssignedSoftwares { get; set; }
        public virtual DbSet<BranchLk> BranchLks { get; set; }
        public virtual DbSet<BranchLookup> BranchLookups { get; set; }
        public virtual DbSet<DellServiceTag> DellServiceTags { get; set; }
        public virtual DbSet<DeviceTypeLk> DeviceTypeLks { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Itasset> Itassets { get; set; }
        public virtual DbSet<LocationLk> LocationLks { get; set; }
        public virtual DbSet<PreferenceLk> PreferenceLks { get; set; }
        public virtual DbSet<RefreshedComputer> RefreshedComputers { get; set; }
        public virtual DbSet<ReplacementReferenceLookup> ReplacementReferenceLookups { get; set; }
        public virtual DbSet<SoftwareKey> SoftwareKeys { get; set; }
        public virtual DbSet<SoftwareLk> SoftwareLks { get; set; }
        public virtual DbSet<SpiceworksInventory> SpiceworksInventories { get; set; }
        public virtual DbSet<SpicworksUpdateRamprocessor> SpicworksUpdateRamprocessors { get; set; }
        public virtual DbSet<SyncEmployee> SyncEmployees { get; set; }
        public virtual DbSet<SynchEmpBranchLookup> SynchEmpBranchLookups { get; set; }
        public virtual DbSet<UserAccessLog> UserAccessLogs { get; set; }
        public virtual DbSet<UserProjectAccess> UserProjectAccesses { get; set; }
        public virtual DbSet<VendorLk> VendorLks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseSqlServer("Server=IS-DEVSERVER;Database=Gambit;Trusted_Connection=True;");
                optionsBuilder.UseSqlServer("Server=BSK-Data\\BSK_SQLServer;Database=Gambit;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<ApplicationLoadTable>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("ApplicationLoadTable");

                entity.Property(e => e.ApplicationName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.DeviceName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.InstallationProductKey)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<AssetReplacementLookup>(entity =>
            {
                entity.ToTable("AssetReplacementLookup");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.HasOne(d => d.NewAssetNavigation)
                    .WithMany(p => p.AssetReplacementLookupNewAssetNavigations)
                    .HasForeignKey(d => d.NewAsset)
                    .HasConstraintName("FK_AssetReplacementLookup_ITAsset1");

                entity.HasOne(d => d.OldAssetNavigation)
                    .WithMany(p => p.AssetReplacementLookupOldAssetNavigations)
                    .HasForeignKey(d => d.OldAsset)
                    .HasConstraintName("FK_AssetReplacementLookup_ITAsset");
            });

            modelBuilder.Entity<AssignedAsset>(entity =>
            {
                entity.ToTable("AssignedAsset");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssignedAssets)
                    .HasForeignKey(d => d.AssetId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AssignedAsset_ITAsset");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.AssignedAssets)
                    .HasForeignKey(d => d.EmployeeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AssignedAsset_Employee");
            });

            modelBuilder.Entity<AssignedSoftware>(entity =>
            {
                entity.ToTable("AssignedSoftware");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.SoftwareKeyId).HasColumnName("SoftwareKeyID");

                entity.HasOne(d => d.Asset)
                    .WithMany(p => p.AssignedSoftwares)
                    .HasForeignKey(d => d.AssetId)
                    .HasConstraintName("FK_AssignedSoftware_ITAsset");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.AssignedSoftwares)
                    .HasForeignKey(d => d.EmployeeId)
                    .HasConstraintName("FK_AssignedSoftware_Employee");

                entity.HasOne(d => d.SoftwareKey)
                    .WithMany(p => p.AssignedSoftwares)
                    .HasForeignKey(d => d.SoftwareKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AssignedSoftware_SoftwareKey");
            });

            modelBuilder.Entity<BranchLk>(entity =>
            {
                entity.ToTable("Branch_lk");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Branch).HasMaxLength(150);
            });

            modelBuilder.Entity<BranchLookup>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("BranchLookup");

                entity.Property(e => e.Lookup)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResultValue)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DellServiceTag>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("DellServiceTag");

                entity.Property(e => e.DeviceType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Device Type");

                entity.Property(e => e.IpAddress)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("IP Address");

                entity.Property(e => e.Location)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperatingSystem)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Operating System");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("Serial Number");
            });

            modelBuilder.Entity<DeviceTypeLk>(entity =>
            {
                entity.ToTable("DeviceType_lk");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Desciption)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.ToTable("Employee");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.ObjectId)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Itasset>(entity =>
            {
                entity.ToTable("ITAsset");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.Bskponumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("BSKPONumber");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DeviceTypeId).HasColumnName("DeviceTypeID");

                entity.Property(e => e.IsDecommissioned).HasDefaultValueSql("((0))");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperatingSystem)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PurchasedOn).HasColumnType("datetime");

                entity.Property(e => e.SerialNumber).HasMaxLength(100);

                entity.Property(e => e.ServiceTagNumber).HasMaxLength(100);

                entity.Property(e => e.Size)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.VendorId).HasColumnName("VendorID");

                entity.Property(e => e.VendorPonumber)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("VendorPONumber");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.Itassets)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ITAsset_Branch_lk");

                entity.HasOne(d => d.DeviceType)
                    .WithMany(p => p.Itassets)
                    .HasForeignKey(d => d.DeviceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ITAsset_DeviceType_lk");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Itassets)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK_ITAsset_Location_lk");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.Itassets)
                    .HasForeignKey(d => d.VendorId)
                    .HasConstraintName("FK_ITAsset_Vendor_lk");
            });

            modelBuilder.Entity<LocationLk>(entity =>
            {
                entity.ToTable("Location_lk");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PreferenceLk>(entity =>
            {
                entity.ToTable("Preference_lk");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Preference)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreferenceValue)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefreshedComputer>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("RefreshedComputers");

                entity.Property(e => e.AssetId).HasColumnName("AssetID");

                entity.Property(e => e.Branch).HasMaxLength(150);

                entity.Property(e => e.Desciption)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Ds)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("DS");

                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OperatingSystem)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PurchasedOn).HasColumnType("datetime");

                entity.Property(e => e.SerialNumber).HasMaxLength(100);

                entity.Property(e => e.ServiceTagNumber).HasMaxLength(100);

                entity.Property(e => e.Size)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ReplacementReferenceLookup>(entity =>
            {
                entity.ToTable("ReplacementReferenceLookup");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NewAssetId).HasColumnName("NewAssetID");

                entity.Property(e => e.OldAssetId).HasColumnName("OldAssetID");
            });

            modelBuilder.Entity<SoftwareKey>(entity =>
            {
                entity.ToTable("SoftwareKey");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.LicenseKey).HasMaxLength(500);

                entity.Property(e => e.Notes).HasMaxLength(500);

                entity.Property(e => e.PurchaseDate).HasColumnType("datetime");

                entity.Property(e => e.SoftwareId).HasColumnName("SoftwareID");

                entity.Property(e => e.VendorId).HasColumnName("VendorID");
            });

            modelBuilder.Entity<SoftwareLk>(entity =>
            {
                entity.ToTable("Software_lk");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<SpiceworksInventory>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SpiceworksInventory");

                entity.Property(e => e.DeviceType)
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasColumnName("Device Type");

                entity.Property(e => e.LastLogin)
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .HasColumnName("Last Login");

                entity.Property(e => e.Location)
                    .HasMaxLength(23)
                    .IsUnicode(false);

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(21)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(38)
                    .IsUnicode(false);

                entity.Property(e => e.OperatingSystem)
                    .HasMaxLength(32)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SpicworksUpdateRamprocessor>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("SpicworksUpdateRAMProcessor");

                entity.Property(e => e.IpAddress)
                    .HasMaxLength(550)
                    .IsUnicode(false)
                    .HasColumnName("IP Address");

                entity.Property(e => e.LastLogin)
                    .HasMaxLength(550)
                    .IsUnicode(false)
                    .HasColumnName("Last Login");

                entity.Property(e => e.LastScanTime)
                    .HasMaxLength(550)
                    .IsUnicode(false)
                    .HasColumnName("Last Scan Time");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.Memory)
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.Model)
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(550)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfProcessors)
                    .HasMaxLength(550)
                    .IsUnicode(false)
                    .HasColumnName("Number of Processors");

                entity.Property(e => e.ProcessorType)
                    .HasMaxLength(550)
                    .IsUnicode(false)
                    .HasColumnName("Processor Type");

                entity.Property(e => e.RawProcessorType)
                    .HasMaxLength(550)
                    .IsUnicode(false)
                    .HasColumnName("Raw Processor Type");
            });

            modelBuilder.Entity<SyncEmployee>(entity =>
            {
                entity.ToTable("SyncEmployee");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.Department)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.ObjectId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhysicalDeliveryOfficeName).HasMaxLength(250);
            });

            modelBuilder.Entity<SynchEmpBranchLookup>(entity =>
            {
                entity.ToTable("SynchEmpBranchLookup");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.PhysicalDeliveryOfficeName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAccessLog>(entity =>
            {
                entity.ToTable("UserAccessLog");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.SignInDate).HasColumnType("datetime");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<UserProjectAccess>(entity =>
            {
                entity.ToTable("UserProjectAccess");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AdobjectId)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("ADObjectID");

                entity.Property(e => e.BranchId).HasColumnName("BranchID");

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.ProjectId).HasColumnName("ProjectID");
            });

            modelBuilder.Entity<VendorLk>(entity =>
            {
                entity.ToTable("Vendor_lk");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.VendorName)
                    .IsRequired()
                    .HasMaxLength(150);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
