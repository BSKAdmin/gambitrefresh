﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class UserProjectAccess
    {
        public int Id { get; set; }
        public string AdobjectId { get; set; }
        public int BranchId { get; set; }
        public string GroupName { get; set; }
        public int? ProjectId { get; set; }
        public bool? IsAdmin { get; set; }
    }
}
