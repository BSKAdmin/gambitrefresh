﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class BranchLookup
    {
        public string Lookup { get; set; }
        public string ResultValue { get; set; }
    }
}
