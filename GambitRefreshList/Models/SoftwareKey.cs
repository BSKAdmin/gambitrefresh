﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class SoftwareKey
    {
        public SoftwareKey()
        {
            AssignedSoftwares = new HashSet<AssignedSoftware>();
        }

        public int Id { get; set; }
        public int SoftwareId { get; set; }
        public int? VendorId { get; set; }
        public string LicenseKey { get; set; }
        public string Notes { get; set; }
        public int? NumberOfInstallsAllowed { get; set; }
        public DateTime? PurchaseDate { get; set; }

        public virtual ICollection<AssignedSoftware> AssignedSoftwares { get; set; }
    }
}
