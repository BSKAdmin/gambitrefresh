﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class VendorLk
    {
        public VendorLk()
        {
            Itassets = new HashSet<Itasset>();
        }

        public int Id { get; set; }
        public string VendorName { get; set; }

        public virtual ICollection<Itasset> Itassets { get; set; }
    }
}
