﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class ReplacementReferenceLookup
    {
        public int Id { get; set; }
        public int? OldAssetId { get; set; }
        public int? NewAssetId { get; set; }
    }
}
