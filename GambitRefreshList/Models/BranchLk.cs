﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class BranchLk
    {
        public BranchLk()
        {
            Itassets = new HashSet<Itasset>();
        }

        public int Id { get; set; }
        public string Branch { get; set; }

        public virtual ICollection<Itasset> Itassets { get; set; }
    }
}
