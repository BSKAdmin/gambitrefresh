﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class SoftwareLk
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
