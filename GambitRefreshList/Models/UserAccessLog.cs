﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class UserAccessLog
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public DateTime SignInDate { get; set; }
    }
}
