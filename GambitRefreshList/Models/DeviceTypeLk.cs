﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class DeviceTypeLk
    {
        public DeviceTypeLk()
        {
            Itassets = new HashSet<Itasset>();
        }

        public int Id { get; set; }
        public string Desciption { get; set; }

        public virtual ICollection<Itasset> Itassets { get; set; }
    }
}
