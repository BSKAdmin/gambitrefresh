﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class AssetReplacementLookup
    {
        public int Id { get; set; }
        public int? OldAsset { get; set; }
        public int? NewAsset { get; set; }

        public virtual Itasset NewAssetNavigation { get; set; }
        public virtual Itasset OldAssetNavigation { get; set; }
    }
}
