﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class PreferenceLk
    {
        public long Id { get; set; }
        public string Preference { get; set; }
        public string PreferenceValue { get; set; }
    }
}
