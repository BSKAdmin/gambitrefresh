﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class Employee
    {
        public Employee()
        {
            AssignedAssets = new HashSet<AssignedAsset>();
            AssignedSoftwares = new HashSet<AssignedSoftware>();
        }

        public int Id { get; set; }
        public string ObjectId { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool DoNotProcess { get; set; }
        public int? BranchId { get; set; }

        public virtual ICollection<AssignedAsset> AssignedAssets { get; set; }
        public virtual ICollection<AssignedSoftware> AssignedSoftwares { get; set; }
    }
}
