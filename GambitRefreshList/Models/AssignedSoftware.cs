﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class AssignedSoftware
    {
        public int Id { get; set; }
        public int SoftwareKeyId { get; set; }
        public int? AssetId { get; set; }
        public int? EmployeeId { get; set; }

        public virtual Itasset Asset { get; set; }
        public virtual Employee Employee { get; set; }
        public virtual SoftwareKey SoftwareKey { get; set; }
    }
}
