﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class AssignedAsset
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int AssetId { get; set; }

        public virtual Itasset Asset { get; set; }
        public virtual Employee Employee { get; set; }
    }
}
