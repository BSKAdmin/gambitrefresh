﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class SpiceworksInventory
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string DeviceType { get; set; }
        public string Model { get; set; }
        public string Location { get; set; }
        public string OperatingSystem { get; set; }
        public string LastLogin { get; set; }
    }
}
