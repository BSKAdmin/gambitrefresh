﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class SyncEmployee
    {
        public int Id { get; set; }
        public string ObjectId { get; set; }
        public string Department { get; set; }
        public int? BranchId { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string PhysicalDeliveryOfficeName { get; set; }
    }
}
