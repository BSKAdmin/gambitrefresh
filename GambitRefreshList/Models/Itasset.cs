﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class Itasset
    {
        public Itasset()
        {
            AssetReplacementLookupNewAssetNavigations = new HashSet<AssetReplacementLookup>();
            AssetReplacementLookupOldAssetNavigations = new HashSet<AssetReplacementLookup>();
            AssignedAssets = new HashSet<AssignedAsset>();
            AssignedSoftwares = new HashSet<AssignedSoftware>();
        }

        public int Id { get; set; }
        public int BranchId { get; set; }
        public int? LocationId { get; set; }
        public int DeviceTypeId { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Size { get; set; }
        public string Model { get; set; }
        public string OperatingSystem { get; set; }
        public bool? IsDecommissioned { get; set; }
        public DateTime? DateModified { get; set; }
        public int? VendorId { get; set; }
        public DateTime? PurchasedOn { get; set; }
        public string VendorPonumber { get; set; }
        public string Bskponumber { get; set; }
        public string SerialNumber { get; set; }
        public string ServiceTagNumber { get; set; }
        public int? Replacement { get; set; }
        public int? DoNotRefresh { get; set; }

        public virtual BranchLk Branch { get; set; }
        public virtual DeviceTypeLk DeviceType { get; set; }
        public virtual LocationLk Location { get; set; }
        public virtual VendorLk Vendor { get; set; }
        public virtual ICollection<AssetReplacementLookup> AssetReplacementLookupNewAssetNavigations { get; set; }
        public virtual ICollection<AssetReplacementLookup> AssetReplacementLookupOldAssetNavigations { get; set; }
        public virtual ICollection<AssignedAsset> AssignedAssets { get; set; }
        public virtual ICollection<AssignedSoftware> AssignedSoftwares { get; set; }
    }
}
