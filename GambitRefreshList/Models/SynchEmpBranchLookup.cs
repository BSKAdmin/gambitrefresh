﻿using System;
using System.Collections.Generic;

#nullable disable

namespace GambitRefreshList.Models
{
    public partial class SynchEmpBranchLookup
    {
        public int Id { get; set; }
        public string PhysicalDeliveryOfficeName { get; set; }
        public int BranchId { get; set; }
    }
}
