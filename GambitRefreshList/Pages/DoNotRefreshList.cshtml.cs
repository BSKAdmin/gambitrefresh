﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class DoNotRefreshListModel : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;

        public DoNotRefreshListModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public IList<Itasset> Itasset { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
          
            Itasset = await _context.Itassets
                .Include(i => i.Branch)
                .Include(i => i.DeviceType)
                .Include(i => i.Location)
                .Include(i => i.Vendor).Where(a => a.DoNotRefresh == 1).ToListAsync();
                 
            if (Itasset == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //_context.Attach(AssetReplacementLookup).State = EntityState.Modified;

            try
            {
                var assetid = Request.Form["item.Id"];
            
                //set donotshow flag bit inactive
                var donotshow = _context.Itassets.FirstOrDefault(b => b.Id == Int32.Parse(assetid[0]));
                if (donotshow != null)
                {
                    donotshow.DoNotRefresh = null;
                }

              
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
            }

            return RedirectToPage("./DoNotRefreshList");
        }

    }
}
