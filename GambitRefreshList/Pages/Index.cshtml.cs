﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class IndexModel : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;
        private readonly Itasset itasset;
        

        public IndexModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public List<RefreshedComputer> RefreshedComputer { get;set; }

        public async Task OnGetAsync()
        {


            this.RefreshedComputer = new List<RefreshedComputer>();
            var fulllist =  _context.RefreshedComputers.ToList();
            foreach (var item in fulllist)
            {
                if (!_context.AssetReplacementLookups.Any(e => e.OldAsset == item.AssetId))
                {
                    this.RefreshedComputer.Add(item);
                }                
            }
        }
    }
}