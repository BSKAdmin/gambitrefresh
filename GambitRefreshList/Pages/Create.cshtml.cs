﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class CreateModel : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;

        public CreateModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["Branch"] = new SelectList(_context.BranchLks, "Id", "Branch"); 
            ViewData["Location"] = new SelectList(_context.LocationLks, "Id", "Description");
            ViewData["BranchId"] = new SelectList(_context.BranchLks, "Id", "Id");
            ViewData["EmployeeId"] = new SelectList(_context.Employees, "Id", "DisplayName");
            ViewData["LocationId"] = new SelectList(_context.LocationLks, "Id", "Id");
            ViewData["VendorId"] = new SelectList(_context.VendorLks, "Id", "VendorName");
            ViewData["DeviceTypeDrop"] = new SelectList(_context.DeviceTypeLks, "Id", "Desciption");
            return Page();
        }

        [BindProperty]
        public Itasset Itasset { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //insert qty from model
            try
            {
                int qty = Int32.Parse(Request.Form["Qty"]);
                var laptopflag = "";
                if (Itasset.DeviceTypeId == 2)
                {
                    laptopflag = "L";
                }
               
                if (qty <= 10)
                {
                    for (int i = 0; i <  qty; i++)
                    {
                        //figure out naming convention and apply it
                        using (var context = new GambitContext())
                        {
                            var itasset = new Itasset();
                            var name = _context.PreferenceLks.SingleOrDefault(b => b.Preference == "ComputerNameSchema").PreferenceValue +
                            laptopflag +
                            "-" +
                            context.PreferenceLks.SingleOrDefault(b => b.Preference == "ComputerName").PreferenceValue;
          
                            itasset.Manufacturer = Itasset.Manufacturer;
                            itasset.LocationId = Itasset.LocationId;
                            itasset.Model = Itasset.Model;
                            itasset.BranchId = Itasset.BranchId;
                            itasset.DeviceTypeId = Itasset.DeviceTypeId;
                            itasset.Name = name;
                            itasset.Replacement = 1;
                            itasset.OperatingSystem = Itasset.OperatingSystem;
                            itasset.PurchasedOn = Itasset.PurchasedOn;
                            itasset.Bskponumber = Itasset.Bskponumber;
                            itasset.VendorId = Itasset.VendorId;

                            //add asset
                            context.Itassets.Add(itasset);
                            context.Entry(itasset).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                            //update the end counters  
                            var result = context.PreferenceLks.SingleOrDefault(b => b.Preference == "ComputerName");
                            if (result != null)
                            {
                                int incrementedcomputer = Int32.Parse(result.PreferenceValue) + 1;
                                result.PreferenceValue = $"{incrementedcomputer:0000}";
                                context.Entry(result).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            }
                            context.SaveChanges();
                        }                      
                    }
                }
            }

            catch
            {
                throw;
               //return RedirectToPage("./NotFound");
            }
            return RedirectToPage("./Index");
        }
    }
}
