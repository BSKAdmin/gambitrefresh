﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class AssignedAssetInfoModel : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;

        public AssignedAssetInfoModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public AssetReplacementLookup AssetReplacementLookup { get; set; }
        [BindProperty(SupportsGet = true)] public string OldID { get; set; }
        public async Task<IActionResult> OnGetAsync(int? OldID)
        {

        

            if (OldID == null)
            {
                return RedirectToPage("./NotFound");
            }
            var OldName = await _context.Itassets.FirstAsync(b => b.Id == OldID);
            ViewData["Name"] = OldName.Name;

            AssetReplacementLookup = await _context.AssetReplacementLookups
                .Include(a => a.NewAssetNavigation)
                .Include(a => a.OldAssetNavigation).FirstOrDefaultAsync(m => m.OldAsset == OldID);

            if (AssetReplacementLookup == null)
            {
                return RedirectToPage("./NotFound");
            }
            return Page();
        }
    }
}
