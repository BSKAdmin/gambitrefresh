﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class AssignedReplacementModel : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;
        public IList<Itasset> Itasset { get; set; }
        public IList<AssetReplacementLookup> assets { get; set; }

        public AssignedReplacementModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        [BindProperty]
        public AssetReplacementLookup AssetReplacementLookup { get; set; }
        public int NewAssetId { get; set; }

        public int OldAssetId { get; set; }
        public async Task<IActionResult> OnGetAsync(int? id)
        {

            assets = await _context.AssetReplacementLookups
                .Include(a => a.NewAssetNavigation)
                .Include(a => a.OldAssetNavigation).ToListAsync();
            if (assets == null)
            {
                return RedirectToPage("./NotFound");
            }


            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            //_context.Attach(AssetReplacementLookup).State = EntityState.Modified;

            try
            {
                var assetid = Request.Form["item.NewAssetNavigation.Id"];
                var oldformid = Request.Form["item.OldAssetNavigation.Id"];
                NewAssetId = Int32.Parse(assetid[0]);
                OldAssetId = Int32.Parse(oldformid[0]);

                //delete old associates
                var all = _context.AssetReplacementLookups.Where(d => d.OldAsset == OldAssetId);

                //set replacement flag bit inactive
                var replacement = _context.Itassets.FirstOrDefault(b => b.Id == NewAssetId);
                if (replacement != null)
                {
                    replacement.Replacement = 1;
                }

                _context.AssetReplacementLookups.RemoveRange(all);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AssetReplacementLookupExists(AssetReplacementLookup.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./AssignedReplacement");
        }

        private bool AssetReplacementLookupExists(int id)
        {
            return _context.AssetReplacementLookups.Any(e => e.Id == id);
        }
    }
}
