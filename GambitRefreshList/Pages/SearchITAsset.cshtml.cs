﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class SearchITAsset : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;

        public SearchITAsset(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public IList<Itasset> Itasset { get; set; }
        public IList<AssetReplacementLookup> assets { get; set; }
        public IList<RefreshedComputer> RefreshedComputer { get; set; }

        [BindProperty]
        public int NewAssetId { get; set; }
        public int OldAssetId { get; set; }

        
        public async Task<IActionResult> OnPostSubmit()
        {
            try
            {
                //add asset
                var assetassignment = new AssetReplacementLookup();
                var assetid = Request.Form["item.Id"];
                var formid = Request.Form["id"];
                NewAssetId = Int32.Parse(assetid[0]);
                OldAssetId = Int32.Parse(formid[0]);

                //delete old associates
                var all = _context.AssetReplacementLookups.Where(d => d.OldAsset == OldAssetId);

                //set replacement flag bit inactive
                var replacement = _context.Itassets.FirstOrDefault(b => b.Id == NewAssetId);
                if (replacement != null)
                {
                    replacement.Replacement = null;
                    await _context.SaveChangesAsync();

                }

                _context.AssetReplacementLookups.RemoveRange(all);
                if (all == null)
                {
                    return RedirectToPage("./NotFound");
                }

                await _context.SaveChangesAsync();

                assetassignment.NewAsset = NewAssetId;
                assetassignment.OldAsset = OldAssetId;

                _context.AssetReplacementLookups.Add(assetassignment);
                _context.Entry(assetassignment).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }
            catch (Exception e)
            {
                return RedirectToPage("./NotFound");
            }
  
        }
        [BindProperty(SupportsGet = true)] public string OldID { get; set; }
        public async Task<IActionResult> OnGetAsync(string OldID)
        {
            Itasset = await _context.Itassets.Where(b => b.Replacement == 1).ToListAsync();   
            OldAssetId = Int32.Parse(OldID);

            var OldName = await _context.Itassets.FirstAsync(b => b.Id == OldAssetId);
            ViewData["OldAssetId"] = OldAssetId;
            ViewData["Name"] = OldName.Name;
            if (OldID == null)
            {
                return RedirectToPage("./NotFound");
            }
            if (Itasset == null)
            {
                return RedirectToPage("./NotFound");
            }
                     
            return Page();
        }
    }
}
