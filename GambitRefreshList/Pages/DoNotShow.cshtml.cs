﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class DoNotShowModel : PageModel
    {
        private readonly GambitRefreshList.Models.GambitContext _context;

        public DoNotShowModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public Itasset Itasset { get; set; }
        [BindProperty(SupportsGet = true)] public int OldID { get; set; }
        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (OldID == null)
            {
                return NotFound();
            }


    

            var donotshow = _context.Itassets.FirstOrDefault(b => b.Id == OldID);
           
            if (donotshow != null)
            {
                donotshow.DoNotRefresh = 1;
                await _context.SaveChangesAsync();

            }
            return RedirectToPage("./Index");
        }
    }
}
