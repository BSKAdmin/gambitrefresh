﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using GambitRefreshList.Models;

namespace GambitRefreshList.Pages
{
    public class CompletedComputerModel : PageModel
    {

        [BindProperty]
        public int NewAssetId { get; set; }
        public int OldAssetId { get; set; }



        private readonly GambitRefreshList.Models.GambitContext _context;

        public CompletedComputerModel(GambitRefreshList.Models.GambitContext context)
        {
            _context = context;
        }

        public IList<AssetReplacementLookup> AssetReplacementLookup { get;set; }

        public async Task OnGetAsync()
        {
            AssetReplacementLookup = await _context.AssetReplacementLookups
                .Include(a => a.NewAssetNavigation)
                .Include(a => a.OldAssetNavigation).ToListAsync();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                //add asset
                var assetid = Request.Form["item.NewAssetNavigation.Id"];
                var oldformid = Request.Form["item.OldAssetNavigation.Id"];
                NewAssetId = Int32.Parse(assetid[0]);
                OldAssetId = Int32.Parse(oldformid[0]);

                //find associate record owner
                //insert new owner association
                var replacement = _context.AssignedAssets.FirstOrDefault(b => b.AssetId == OldAssetId);
                var previousperson = replacement.EmployeeId;
               // var previousid = replacement.AssetId;
                if (replacement != null)
                {
                    replacement.EmployeeId = 322;
                    await _context.SaveChangesAsync();

                }

                //set current employee in it asset
                /*
                var newasset = _context.Itassets.FirstOrDefault(b => b.Id == NewAssetId);
               
                // var previousid = replacement.AssetId;
                if (replacement != null)
                {
                    newasset.EmployeeId = previousperson;
                    await _context.SaveChangesAsync();

                }
                */

                AssignedAsset assignedowner = new AssignedAsset();
                assignedowner.EmployeeId = previousperson;
                assignedowner.AssetId = NewAssetId;
                await _context.SaveChangesAsync();

                _context.AssignedAssets.Add(assignedowner);
                _context.Entry(assignedowner).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                
                
             

                var deploy = _context.AssetReplacementLookups.FirstOrDefault(b => b.OldAssetNavigation.Id == OldAssetId);
                if (deploy != null)
                {
                    _context.AssetReplacementLookups.RemoveRange(deploy);
                }
                await _context.SaveChangesAsync();

                //return Page();
                return RedirectToPage("./CompletedComputer");
            }
            catch (Exception e)
            {
                return RedirectToPage("./NotFound");
            }

        }
    }
}
